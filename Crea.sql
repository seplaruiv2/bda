// CREACION DE TABLAS
create table alumnos
(
 dni            varchar2(9) constraint pk_alumnos primary key,
 nombre         varchar2(20),
 apellido1      varchar2(20),
 apellido2      varchar2(20),
 direccion      varchar2(30),
 sexo           varchar2(1),
 fechanac       date
);

create table profesores
(
 dni            varchar2(9) constraint pk_profesores primary key,
 nombre         varchar2(20),
 apellido1      varchar2(20),
 apellido2      varchar2(20),
 direccion      varchar2(30),
  titulo         varchar2(30),
 sueldo number(6)
);

create table cursos
(
 codigocurso    varchar2(5) constraint pk_cursos primary key,
 nombrecurso  varchar2(30),
 maxalumnos     number(4),
 fechaini       date,
 fechafin       date,
 numhoras       number(4),
 profesor       varchar2(9) constraint fk_profesores references profesores
);

create table oposiciones
(
 codigo         varchar2(6) constraint pk_oposiciones primary key,
 nombre         varchar2(30),
 fechaexamen    date,
 organismo      varchar2(30),
 plazas         number(4),
 categoria      varchar2(1) constraint cat_val check (categoria in ('A','B','C','D','E'))
);


create table cursooposicion
(
 codcurso       varchar2(5) constraint fk_cursos2 references cursos,
 codoposicion   varchar2(6) constraint fk_oposicion references oposiciones,
 constraint pk_cursooposicion primary key(codcurso, codoposicion)
);

create table matriculas
(
 dnialumno      varchar2(9)     constraint fk_alumnos references alumnos,
 codcurso       varchar2(5)     constraint fk_cursos references cursos,
 pruebaA        number(2)       constraint pruebaA_val check (pruebaA between 0 and 50),
 pruebaB        number(2)       constraint pruebaB_val check (pruebaB between 0 and 50),
 tipo           varchar2(7)     constraint tipo_val check (tipo in ('Oficial','Libre')),
 inscripcion    date,
 constraint pk_matriculas primary key (dnialumno, codcurso)
);


