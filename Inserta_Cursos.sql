// ALUMNOS (dni, nombre, apellido1, apellido2, direccion, sexo, fechanac)

insert into alumnos
values('1111','Carlos','Puerta','Perez','c/ P?, 21','V','12/09/1989');

insert into alumnos
values('2222','Luisa','Sanchez','Donoso','c/ Sierpes, 1','M','12/05/1968');

insert into alumnos
values('3333','Eva','Ramos','Prieto','c/ Rueda, 31','M','12/04/1969');

insert into alumnos
values('4444','Luis','Paez','Garcia','c/ Martin Villa, 21','V','22/04/1979');

insert into alumnos
values('5555','Ana','Padilla','Torres','c/ Tetuan, 2','M','12/09/1970');

insert into alumnos
values('6666','Lola','Flores','Ruiz','c/ Real, 14','M','18/04/1970');

// PROFESORES (dni, nombre, apellido1, apellido2, direccion, titulo, sueldo)

insert into profesores
values('111','Manuel','Lopez','Garcia','c/ Albeniz,12','Ingeniero de Caminos',2000);

insert into profesores
values('222','Luis','Perez','Sanchez','c/ Huelva, 1','Licenciado en Psicologia',1400);

insert into profesores
values('333','Ana','Garcia','Lopez','c/ Sevilla,2','Ingeniero de Caminos',2200);

insert into profesores
values('444','Eva','Parra','Ruiz','c/ Astoria,7','Licenciado en Derecho',1200);

insert into profesores
values('555','Federico','Flores','Alba','c/ Tarifa, 1','Ingeniero Inform?tico',2500);

insert into profesores
values('666','Alberto','Moreno','Rodriguez','c/ Parra, 2','Ingeniero de Caminos',2100);


// CURSOS  (codigocurso,nombrecurso,maxalumnos,fechaini,fechafin,numhoras,profesor)

insert into cursos 
values('0001','Función Publica',120,'03/05/09','30/06/09',400,'444');

insert into cursos
values('0002','Los chiquillos',180,'13/05/09','30/08/09',600,'222');

insert into cursos
values('0003','Puentes Atirantados',20,'03/12/08','30/06/09',800,'111');

insert into cursos
values('0004','Vida familiar de los presos',120,'03/05/09','30/06/09',400,'222');

insert into cursos
values('0005','La Constitucion',230,'03/05/09','30/06/09',100,'444');

insert into cursos
values('0006','Programacion Visual para todos',80,'03/09/09','30/09/09',30,'555');


--MATRICULAS
insert into matriculas (dnialumno, codcurso, pruebaA, pruebaB, tipo, inscripcion)
values('1111','0001',12,8,'Oficial','12/06/06');

insert into matriculas (dnialumno, codcurso, pruebaA, pruebaB, tipo, inscripcion)
values('1111','0005',18,5,'Oficial','12/07/06');

insert into matriculas (dnialumno, codcurso, pruebaA, pruebaB, tipo, inscripcion)
values('2222','0003',25,28,'Libre','12/08/06');

insert into matriculas (dnialumno, codcurso, pruebaA, pruebaB, tipo, inscripcion)
values('2222','0005',32,28,'Libre','12/09/06');

insert into matriculas (dnialumno, codcurso, pruebaA, tipo, inscripcion)
values('3333','0006',12,'Oficial','12/10/06');

insert into matriculas (dnialumno, codcurso, pruebaB, tipo, inscripcion)
values('4444','0006',18,'Oficial','12/11/06');

insert into matriculas (dnialumno, codcurso, pruebaA, pruebaB, tipo, inscripcion)
values('5555','0006',20,48,'Oficial','12/12/06');

insert into matriculas (dnialumno, codcurso, pruebaA, pruebaB, tipo, inscripcion)
values('5555','0002',32,38,'Libre','12/01/07');

insert into matriculas (dnialumno, codcurso, pruebaA, pruebaB, tipo, inscripcion)
values('5555','0003',11,18,'Oficial','12/02/07');

insert into matriculas (dnialumno, codcurso, pruebaA, pruebaB, tipo, inscripcion)
values('5555','0001',11,38,'Oficial','12/03/07');

insert into matriculas (dnialumno, codcurso, pruebaA, pruebaB, tipo, inscripcion)
values('5555','0005',42,48,'Oficial','12/04/07');


// OPOSICIONES (codigo, nombre, fechaexamen, organismo, plazas, categoria)

insert into oposiciones
values('C-502','Maestros de Primaria','27/05/10','Consejeria Educación', 1220, 'B');

insert into oposiciones
values('C-512','Funcionario de Prisiones','20/06/10','Consejeria Justicia', 120, 'C');

insert into oposiciones
values('C-522','Profesores de Inform?tica','27/06/09','Consejeria Educación', 12, 'A');

insert into oposiciones
values('C-532','Jardineros del Estado','27/05/10','Ministerio Medio Ambiente', 10, 'D');

insert into oposiciones
values('C-542','Administrativos','27/05/10','Ayuntamiento DH', 12, 'C');

insert into oposiciones
values('C-552','Ingenieros del Ejercito','27/09/10','Ministerio Defensa', 120, 'A');





// CURSOOPOSICION  (codcurso, codoposicion)

insert into cursooposicion
values('0001','C-502');

insert into cursooposicion
values('0001','C-512');

insert into cursooposicion
values('0001','C-522');

insert into cursooposicion
values('0001','C-532');

insert into cursooposicion
values('0001','C-542');

insert into cursooposicion
values('0001','C-552');

insert into cursooposicion
values('0002','C-502');

insert into cursooposicion
values('0003','C-552');

insert into cursooposicion
values('0004','C-512');

insert into cursooposicion
values('0006','C-522');

insert into cursooposicion
values('0005','C-502');

insert into cursooposicion
values('0005','C-512');

insert into cursooposicion
values('0005','C-522');

insert into cursooposicion
values('0005','C-532');

insert into cursooposicion
values('0005','C-542');